package tests;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import character.Mage;
import character.Ranger;
import character.Rogue;
import character.Warrior;
import items.Armor;
import items.Weapons;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

class CharacterTest {

    private Mage mage;
    private Ranger ranger;
    private Rogue rogue;
    private Warrior warrior;


    @Test
    void when_character_created_then_level_should_be_1() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        int expectedlevel = 1;

        Assertions.assertEquals(mage.getCharacterLvl(), expectedlevel);

    }

    @Test
    void when_character_created_then_level_should_be_2() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        mage.lvl_up();
        int expectedlevel = 2;

        Assertions.assertEquals(mage.getCharacterLvl(), expectedlevel);

    }

    @Test
    void default_primary_attributes_should_be_set_for_mage() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        mage.getBasePrmy_attributes();
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        Assertions.assertEquals(mage.getBasePrmy_attributes().getStrength(), expectedStrength);
        Assertions.assertEquals(mage.getBasePrmy_attributes().getDexterity(), expectedDexterity);
        Assertions.assertEquals(mage.getBasePrmy_attributes().getIntelligence(), expectedIntelligence);

    }

    @Test
    void default_primary_attributes_should_be_set_for_ranger() throws InvalidArmorException, InvalidWeaponException {
        Ranger ranger = new Ranger("Ranger", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        ranger.baseLvl();
        ranger.getBasePrmy_attributes();
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        Assertions.assertEquals(ranger.getBasePrmy_attributes().getStrength(), expectedStrength);
        Assertions.assertEquals(ranger.getBasePrmy_attributes().getDexterity(), expectedDexterity);
        Assertions.assertEquals(ranger.getBasePrmy_attributes().getIntelligence(), expectedIntelligence);


    }

    @Test
    void default_primary_attributes_should_be_set_for_rogue() throws InvalidArmorException, InvalidWeaponException {
        Rogue rogue = new Rogue("Rogue", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        rogue.baseLvl();
        rogue.getBasePrmy_attributes();
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        Assertions.assertEquals(rogue.getBasePrmy_attributes().getStrength(), expectedStrength);
        Assertions.assertEquals(rogue.getBasePrmy_attributes().getDexterity(), expectedDexterity);
        Assertions.assertEquals(rogue.getBasePrmy_attributes().getIntelligence(), expectedIntelligence);

    }

    @Test
    void default_primary_attributes_should_be_set_for_warrior() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Warrior", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        warrior.baseLvl();
        warrior.getBasePrmy_attributes();
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        Assertions.assertEquals(warrior.getBasePrmy_attributes().getStrength(), expectedStrength);
        Assertions.assertEquals(warrior.getBasePrmy_attributes().getDexterity(), expectedDexterity);
        Assertions.assertEquals(warrior.getBasePrmy_attributes().getIntelligence(), expectedIntelligence);

    }

    @Test
    void when_mage_levels_up_then_each_strength_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        mage.lvl_up();

        int expectedNumForStrength = 2;

        Assertions.assertEquals(mage.getBasePrmy_attributes().getStrength(), expectedNumForStrength);

    }

    @Test
    void when_mage_levels_up_then_each_dexterity_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        mage.lvl_up();

        int expectedNumForDexterity = 2;

        Assertions.assertEquals(mage.getBasePrmy_attributes().getDexterity(), expectedNumForDexterity);

    }

    @Test
    void when_mage_levels_up_then_each_intelligence_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Mage mage = new Mage("Mage", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        mage.baseLvl();
        mage.lvl_up();

        int expectedNumForIntelligence = 13;

        Assertions.assertEquals(mage.getBasePrmy_attributes().getIntelligence(), expectedNumForIntelligence);

    }

    @Test
    void when_rogue_levels_up_then_each_strength_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Rogue rogue = new Rogue("Rogue", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        rogue.baseLvl();
        rogue.lvl_up();

        int expectedNumForStrength = 3;

        Assertions.assertEquals(rogue.getBasePrmy_attributes().getStrength(), expectedNumForStrength);
    }

    @Test
    void when_rogue_levels_up_then_each_dexterity_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Rogue rogue = new Rogue("Rogue", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        rogue.baseLvl();
        rogue.lvl_up();

        int expectedNumForDexterity = 10;

        Assertions.assertEquals(rogue.getBasePrmy_attributes().getDexterity(), expectedNumForDexterity);
    }
    @Test
    void when_rogue_levels_up_then_each_Intelligence_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Rogue rogue = new Rogue("Rogue", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        rogue.baseLvl();
        rogue.lvl_up();

        int expectedNumForIntelligence = 2;

        Assertions.assertEquals(rogue.getBasePrmy_attributes().getIntelligence(), expectedNumForIntelligence);
    }
    @Test
    void when_ranger_levels_up_then_each_strength_is_increased() throws InvalidArmorException, InvalidWeaponException {
    Ranger ranger = new Ranger("Ranger",1,0, new BasePrmy_attributes(0,0,0),
            new TotalPrimaryAttributes(0,0,0),
            new Weapons(2, new HashMap<>(),0),
            new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        ranger.baseLvl();
        ranger.lvl_up();

        int expectedNumForStrength = 2;

        Assertions.assertEquals(ranger.getBasePrmy_attributes().getStrength(), expectedNumForStrength);
    }
    @Test
    void when_ranger_levels_up_then_each_dexterity_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Ranger ranger = new Ranger("Ranger",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        ranger.baseLvl();
        ranger.lvl_up();

        int expectedNumForDexterity = 12;

        Assertions.assertEquals(ranger.getBasePrmy_attributes().getDexterity(), expectedNumForDexterity);
    }
    @Test
    void when_ranger_levels_up_then_each_Intelligence_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Ranger ranger = new Ranger("Ranger",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        ranger.baseLvl();
        ranger.lvl_up();

        int expectedNumForIntelligence = 2;

        Assertions.assertEquals(ranger.getBasePrmy_attributes().getIntelligence(), expectedNumForIntelligence);
    }
    @Test
    void when_warrior_levels_up_then_each_Strength_is_increased() throws InvalidArmorException, InvalidWeaponException {
    Warrior warrior = new Warrior("Warrior",1,0, new BasePrmy_attributes(0,0,0),
            new TotalPrimaryAttributes(0,0,0),
            new Weapons(2, new HashMap<>(),0),
            new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        warrior.baseLvl();
        warrior.lvl_up();

        int expectedNumForStrength = 8;

        Assertions.assertEquals(warrior.getBasePrmy_attributes().getStrength(), expectedNumForStrength);
    }
    @Test
    void when_warrior_levels_up_then_each_Dexterity_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Warrior",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        warrior.baseLvl();
        warrior.lvl_up();

        int expectedNumForDexterity = 4;

        Assertions.assertEquals(warrior.getBasePrmy_attributes().getDexterity(), expectedNumForDexterity);
    }
    @Test
    void when_warrior_levels_up_then_each_Intelligence_is_increased() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Warrior",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

        warrior.baseLvl();
        warrior.lvl_up();

        int expectedNumForIntelligence = 2;

        Assertions.assertEquals(warrior.getBasePrmy_attributes().getIntelligence(), expectedNumForIntelligence);
    }
}
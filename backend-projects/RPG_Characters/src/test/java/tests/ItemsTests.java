package tests;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;
import character.Warrior;
import items.Armor;
import items.Weapons;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;


class ItemsTests {

    Warrior warrior;



    @BeforeEach
    void setUp() {
        warrior = new Warrior("Warrior", 1, 0, new BasePrmy_attributes(5, 2, 1),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

    }

    @Test
    void InvalidWeaponException_character_should_not_be_able_to_equip_higher_level_weapon_ThrowException() throws InvalidWeaponException {
        warrior.getWeapons().setRequiredLvl(1);
        String expected = "Your level is too low or you are probably choosing a wrong weapon, Warrior can only use: "
                + WeaponTypes.Swords + "or" + WeaponTypes.Axes + "or" + WeaponTypes.Hammers +
                "and the equipments are "  + Equipments.Weapon;


        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, warrior::addWeaponsAttributes);
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void InvalidArmorException_character_should_not_be_able_to_equip_higher_level_armor_ThrowException() throws InvalidArmorException {
        warrior.getArmor().setRequiredLvl(1);
        String expected = "Your level is too low or You cannot get that armor, " +
                "Warrior can only have: " + ArmorTypes.Mail +
                ArmorTypes.Plate;


        InvalidArmorException exception = assertThrows(InvalidArmorException.class, warrior::addArmorAttributes);
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void InvalidWeaponException_when_character_equips_wrong_weapon_type_exception_should_be_thrown_ThrowException() throws InvalidWeaponException {

        warrior.getWeapons().getEquipmentsWeapon().put(Equipments.Weapon, WeaponTypes.Staffs);
        String expected = "Your level is too low or you are probably choosing a wrong weapon, Warrior can only use: "
                + WeaponTypes.Swords + "or" + WeaponTypes.Axes + "or" + WeaponTypes.Hammers +
                "and the equipments are " + Equipments.Weapon;

        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, warrior::addWeaponsAttributes);
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void InvalidArmorException_when_character_equips_wrong_armor_type_exception_should_be_thrown_ThrowException() throws InvalidArmorException {

        warrior.getArmor().getEquipmentsArmor().put(Equipments.Weapon, ArmorTypes.Cloth);
        String expected = "Your level is too low or You cannot get that armor, " +
                "Warrior can only have: " + ArmorTypes.Mail +
                ArmorTypes.Plate;

        InvalidArmorException exception = assertThrows(InvalidArmorException.class, warrior::addArmorAttributes);
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void AddWeaponAttributes_when_character_equips_right_weapon_True() throws InvalidWeaponException {

        warrior.addWeaponsAttributes();

        boolean ifWeaponIsValid = warrior.getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Axes) ||
                warrior.getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Hammers) ||
                warrior.getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords);

        assertTrue(ifWeaponIsValid);


    }

    @Test
    void AddArmorAttributes_when_character_equips_right_armor_True() throws InvalidArmorException {

        warrior.addArmorAttributes();

        boolean ifArmorIsValid = warrior.getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Mail) ||
                warrior.getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Plate);

        assertTrue(ifArmorIsValid);

    }

    @Test
    void CheckingCharactersAttributes_calculate_DPS_if_no_weapon_is_equipped_True() {

        warrior.checkingCharactersAttributes();
        warrior.totalCharactersDPS();

        assertEquals(warrior.getCharactersDPS(), 1.05);
    }

    @Test
    void TotalCharacterDPS_calculate_DPS_if_valid_weapon_is_equipped_True() throws InvalidWeaponException {

        warrior.addWeaponsAttributes();
        warrior.totalCharactersDPS();


        assertEquals(warrior.getCharactersDPS(), 393.75);

    }

    @Test
    void TotalCharacterDPS_calculate_DPS_if_valid_weapon_and_armor_are_equipped_True() throws InvalidWeaponException, InvalidArmorException {
        warrior.checkingCharactersAttributes();

        warrior.addWeaponsAttributes();
        warrior.addArmorAttributes();
        warrior.totalCharactersDPS();


        //Assert
        assertEquals(warrior.getCharactersDPS(), 444.0);
    }


}



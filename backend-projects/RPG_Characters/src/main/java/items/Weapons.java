package items;

import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;

import java.util.HashMap;

public class Weapons extends Item{

    //initialising weaponsDPS
    private double weaponDPS;

    //Constructors


    public Weapons(int requiredLvl, HashMap<Equipments, WeaponTypes> equipmentsWeapon, double weaponDPS) {
        super(requiredLvl, equipmentsWeapon);
        this.weaponDPS = weaponDPS;
    }

    //getters and setters
    public double getWeaponDPS() {
        return weaponDPS;
    }

    public void setWeaponDPS(double weaponDPS) {
        this.weaponDPS = weaponDPS;
    }

    //toString
    @Override
    public String toString() {
        return "Weapons{" +
                ", weaponDPS=" + weaponDPS +
                ", name='" + name + '\'' +
                ", RequiredLvl=" + RequiredLvl +
                ", equipmentsArmor=" + equipmentsArmor +
                ", equipmentsWeapon=" + equipmentsWeapon +
                '}';
    }
}

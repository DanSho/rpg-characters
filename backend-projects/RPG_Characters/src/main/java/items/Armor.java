package items;

import Attributes.BasePrmy_attributes;
import Types.ArmorTypes;
import Types.Equipments;

import java.util.HashMap;

public class Armor extends Item {

   protected ArmorTypes armorTypes;
    protected BasePrmy_attributes basePrmy_attributes;

    // constructors
    public Armor(String name, int requiredLvl, HashMap<Equipments, ArmorTypes> equipmentsArmor, BasePrmy_attributes basePrmy_attributes) {
        super(name, requiredLvl, equipmentsArmor);

        this.basePrmy_attributes = basePrmy_attributes;
    }

    //Getters and setters
    public BasePrmy_attributes getBasePrmy_attributes () {
        return basePrmy_attributes;
    }

    public void setPrimaryAttributes(BasePrmy_attributes basePrmy_attributes) {
        this.basePrmy_attributes = basePrmy_attributes;
    }

    public ArmorTypes getArmorTypes() {
       return armorTypes;
    }

    public void setArmorTypes(ArmorTypes armorTypes) {
        this.armorTypes = armorTypes;
    }


    @Override
    public String toString() {
        return "Armor{" +
                ", basePrmy_attributes=" + basePrmy_attributes +
                ", name='" + name + '\'' +
                ", RequiredLvl=" + RequiredLvl +
                ", equipmentsArmor=" + equipmentsArmor +
                '}';
    }
}

package items;

import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;

import java.util.HashMap;

public abstract class Item {

    protected String name;
    protected int RequiredLvl;

    //Hashmap which basically stores items-key/value
    protected HashMap<Equipments, ArmorTypes> equipmentsArmor;
    protected HashMap<Equipments, WeaponTypes> equipmentsWeapon;

    //constructors
    public Item(String name, int requiredLvl, HashMap<Equipments, ArmorTypes> equipmentsArmor) {
        this.name = name;
        this.RequiredLvl = requiredLvl;
        this.equipmentsArmor = equipmentsArmor;

    }

    public Item(int requiredLvl, HashMap<Equipments, WeaponTypes> equipmentsWeapon) {
        this.RequiredLvl = requiredLvl;
        this.equipmentsWeapon = equipmentsWeapon;
    }

//Getters and setters

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLvl() {
        return RequiredLvl;
    }

    public void setRequiredLvl(int requiredLvl) {
        this.RequiredLvl = requiredLvl;
    }

    public HashMap<Equipments, ArmorTypes> getEquipmentsArmor() {
        return equipmentsArmor;
    }

    public void setEquipmentsArmor(HashMap<Equipments, ArmorTypes> equipmentsArmor) {
        this.equipmentsArmor = equipmentsArmor;
    }

    public HashMap<Equipments, WeaponTypes> getEquipmentsWeapon() {
        return equipmentsWeapon;
    }

    public void setEquipmentsWeapon(HashMap<Equipments, WeaponTypes> equipmentsWeapon) {
        this.equipmentsWeapon = equipmentsWeapon;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", RequiredLvl=" + RequiredLvl +
                ", equipmentsArmor=" + equipmentsArmor +
                ", equipmentsWeapon=" + equipmentsWeapon +
                '}';
    }
}
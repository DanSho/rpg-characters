import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import character.Mage;
import character.Ranger;
import character.Rogue;
import character.Warrior;
import items.Armor;
import items.Weapons;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {
       /*Mage mage = new Mage("Mage",1,0, new BasePrmy_attributes(0,0,0),
               new TotalPrimaryAttributes(0,0,0),
               new Weapons(2, new HashMap<>(),0),
               new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));

       mage.baseLvl();
       mage.lvl_up();
       mage.totalCharactersDPS();
        mage.addArmorAttributes();
        mage.totalAttSum();
        mage.addWeaponsAttributes();
        mage.lvl_up();
     System.out.println(mage);

        Ranger ranger = new Ranger("Ranger",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));


        ranger.baseLvl();
        ranger.lvl_up();
        ranger.totalCharactersDPS();
        ranger.addArmorAttributes();
        mage.totalAttSum();
        ranger.addWeaponsAttributes();
        ranger.lvl_up();
        System.out.println(ranger);


        Rogue rogue = new Rogue("Rogue",1,0, new BasePrmy_attributes(0,0,0),
                new TotalPrimaryAttributes(0,0,0),
                new Weapons(2, new HashMap<>(),0),
                new Armor("Armor", 2, new HashMap<>(),new BasePrmy_attributes(0,0,0)));


        rogue.baseLvl();
        rogue.lvl_up();
        rogue.totalCharactersDPS();
        rogue.addArmorAttributes();
        rogue.totalAttSum();
        rogue.addWeaponsAttributes();
        rogue.lvl_up();
        System.out.println(rogue);
*/

        Warrior warrior = new Warrior("Warrior", 1, 0, new BasePrmy_attributes(0, 0, 0),
                new TotalPrimaryAttributes(0, 0, 0),
                new Weapons(2, new HashMap<>(), 0),
                new Armor("Armor", 2, new HashMap<>(), new BasePrmy_attributes(0, 0, 0)));

        warrior.baseLvl();
        System.out.println(warrior);
        warrior.checkingCharactersAttributes();
        warrior.lvl_up();
        System.out.println(warrior);

        warrior.addWeaponsAttributes();
        System.out.println(warrior);
        warrior.addArmorAttributes();
        warrior.totalAttSum();
        warrior.totalCharactersDPS();

//        System.out.println(warrior);
//     warrior.lvl_up();
//     warrior.lvl_up();
        //  System.out.println(warrior);
        warrior.displayCharacter();
    }
}
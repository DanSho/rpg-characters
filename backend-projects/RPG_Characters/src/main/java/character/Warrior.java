package character;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;
import items.Armor;
import items.Weapons;

public class Warrior extends Character {


    double starting_strength = 5.0;
    double starting_dexterity = 2.0;
    double starting_intelligence = 1.0;


    public Warrior(String name, int characterLvl, double charactersDPS, BasePrmy_attributes basePrmy_attributes, TotalPrimaryAttributes totalPrimaryAttributes, Weapons weapons, Armor armor) {
        super(name, characterLvl, charactersDPS, basePrmy_attributes, totalPrimaryAttributes, weapons, armor);
    }


    public void baseLvl() {
        basePrmy_attributes.setStrength(starting_strength);
        basePrmy_attributes.setDexterity(starting_dexterity);
        basePrmy_attributes.setIntelligence(starting_intelligence);
        getWeapons().setRequiredLvl(2);

    }


    //Level increases by 1 with each call to the function. Each time the is invoked then the level rises by 1 and the value of the baseAttribute increases.
    //Verifies a characters access to weapon, if true, then do the calculation to get characterDPS (for weapon)
    @Override
    public void lvl_up() {
        characterLvl++;

        basePrmy_attributes.setStrength(basePrmy_attributes.getStrength() + 3.0);
        basePrmy_attributes.setDexterity(basePrmy_attributes.getDexterity() + 2.0);
        basePrmy_attributes.setIntelligence(basePrmy_attributes.getIntelligence() + 1.0);

        if (getWeapons().getEquipmentsWeapon().size() > 0) {
            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Hammers)) {
                WeaponTypes.Hammers.setDamage(WeaponTypes.Hammers.getDamage() + 3 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 3 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 3 / 100 * WeaponTypes.Hammers.getAttackPerSec());

            } else if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords)) {
                WeaponTypes.Swords.setDamage(WeaponTypes.Swords.getDamage() + 3 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 3 / 100 * WeaponTypes.Swords.getAttackPerSec());

            } else {
                WeaponTypes.Axes.setDamage(WeaponTypes.Axes.getDamage() + 3 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 3 / 100 * WeaponTypes.Axes.getAttackPerSec());
            }
            weapons.setWeaponDPS(weapons.getWeaponDPS());
            totalCharactersDPS();
        }

        //Basically if the attributes are equal to 0 then set the totalprimary attributes by getting it from the basePrmy_attributes
        //else just add the numbers 3, 2, 1 to each attribute
        if (this.getTotalPrimaryAttributes().getStrength() == 0 &&
                this.getTotalPrimaryAttributes().getDexterity() == 0 &&
                this.getTotalPrimaryAttributes().getIntelligence() == 0) {
            totalPrimaryAttributes.setStrength(basePrmy_attributes.getStrength());
            totalPrimaryAttributes.setDexterity(basePrmy_attributes.getDexterity());
            totalPrimaryAttributes.setIntelligence(basePrmy_attributes.getIntelligence());
        } else {
            this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + 3);
            this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + 2);
            this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + 1);


        }


        System.out.println("You leveled up");

    }

    //checks the size first. if right armor is chosen then do the for loop and also check if character is level 2 or more. The stream().map basically allows you to convert object to something else
    // throws exception if the newly added armor attribute is invalid or if level is too low.
    @Override
    public void addArmorAttributes() throws InvalidArmorException {
        if (getArmor().getEquipmentsArmor().size() == 0) {
            armor.getEquipmentsArmor().put(Equipments.Head, ArmorTypes.Mail);
            armor.getEquipmentsArmor().put(Equipments.Legs, ArmorTypes.Plate);
            armor.getEquipmentsArmor().put(Equipments.Body, ArmorTypes.Plate);
        }


        if (getName().equals("Warrior") && getArmor().getRequiredLvl() >= 2 &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Leather) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Cloth) &&
                !getArmor().getEquipmentsArmor().containsKey(Equipments.Weapon)) {
            for (int j = 0; j < getArmor().getEquipmentsArmor().size(); j++) {
                this.getArmor().getBasePrmy_attributes().setStrength(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getStrength).sum());
                this.getArmor().getBasePrmy_attributes().setDexterity(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getDexterity).sum());
                this.getArmor().getBasePrmy_attributes().setIntelligence(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getIntelligence).sum());

            }
        } else {
            throw new InvalidArmorException("Your level is too low or You cannot get that armor, " +
                    "Warrior can only have: " + ArmorTypes.Mail +
                    ArmorTypes.Plate);

        }
    }


    //Basically just determines the values of the armors overall value: by do the following calculation
    //This method is basically just adding the armor and setting the new character's power based on the armor the character just gained
    //if user chooses another armor other than Mail and plate then an exception is shown with a message
    @Override
    public void totalAttSum() {
        if (!getArmor().getEquipmentsArmor().containsKey(Equipments.Weapon) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Leather) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Cloth)) {

            for (int i = 0; i < getArmor().getEquipmentsArmor().size(); i++) {
                this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + getArmor().getBasePrmy_attributes().getStrength());
                this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + getArmor().getBasePrmy_attributes().getDexterity());
                this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + getArmor().getBasePrmy_attributes().getIntelligence());
            }
        } else {
            throw new NullPointerException("Warrior can only own: " + ArmorTypes.Mail + "or" + ArmorTypes.Plate);
        }
    }

    //Determines whether the added weapons attribute is true, and if so, the determines the weaponsDPS using the value of the added weapon. If the weapon property added is Invalid, an exception is thrown
    @Override
    public void addWeaponsAttributes() throws InvalidWeaponException {
        if (weapons.getEquipmentsWeapon().size() == 0) {
            weapons.getEquipmentsWeapon().put(Equipments.Weapon, WeaponTypes.Swords);
        }
        if (getName().equals("Warrior") && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Daggers) &&
                getWeapons().getRequiredLvl() >= 2 &&
                !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Bows) &&
                !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Wands) &&
                !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Staffs) &&
                !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Head) &&
                !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Legs) &&
                !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Body)) {

            double WeaponValue = weapons.getWeaponDPS();
            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Hammers)) {

                weapons.setWeaponDPS(WeaponTypes.Hammers.getDamage() * WeaponTypes.Hammers.getAttackPerSec() + WeaponValue);

                weapons.setWeaponDPS((weapons.getWeaponDPS() * 100) / 100);

                // totalCharactersDPS();

            } else if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords)) {
                weapons.setWeaponDPS(WeaponTypes.Swords.getDamage() * WeaponTypes.Swords.getAttackPerSec() + WeaponValue);
                weapons.setWeaponDPS((weapons.getWeaponDPS() * 100) / 100);
                // totalCharactersDPS();

            } else {
                weapons.setWeaponDPS(WeaponTypes.Axes.getDamage() * WeaponTypes.Axes.getAttackPerSec() + WeaponValue);
                weapons.setWeaponDPS((weapons.getWeaponDPS() * 100) / 100);
                //  totalCharactersDPS();
            }


        } else {
            throw new InvalidWeaponException("Your level is too low or you are probably choosing a wrong weapon, Warrior can only use: "
                    + WeaponTypes.Swords + "or" + WeaponTypes.Axes + "or" + WeaponTypes.Hammers +
                    "and the equipments are " + Equipments.Weapon);
        }

    }

    //Basically adds weaponDPS to 1 if size is equal to 0 (WeaponDPS)
    @Override
    public void checkingCharactersAttributes() {
        if (weapons.getEquipmentsWeapon().size() == 0) {
            weapons.setWeaponDPS(1);

        }
    }

    //This function calculates the characters DPS.
    @Override
    public void totalCharactersDPS() {
        getBasePrmy_attributes().setStrength(starting_strength);
        getBasePrmy_attributes().setDexterity(starting_dexterity);
        getBasePrmy_attributes().setIntelligence(starting_intelligence);


        if (getArmor().getEquipmentsArmor().size() == 0) {
            charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getStrength() / 100);
        } else {
            charactersDPS = weapons.getWeaponDPS() * (1 +
                    (getBasePrmy_attributes().getStrength() + getArmor().getBasePrmy_attributes().getStrength()) / 100);
            charactersDPS = Math.round((charactersDPS * 100) / 100.0);

        }
        System.out.println(charactersDPS);

    }

    // A toString returns the string representation.
    // And by adding @override then we can return the values of the objects

    @Override
    public String toString() {
        return "Warrior{" +
                "name='" + name + '\'' +
                ", characterLvl=" + characterLvl +
                ", basePrmy_attributes=" + basePrmy_attributes +
                ", totalPrimaryAttributes=" + totalPrimaryAttributes +
                ", weapons=" + weapons +
                ", CharactersDPS=" + charactersDPS +
                ", armor=" + armor +
                '}';
    }
}
package character;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;


import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;
import items.Armor;
import items.Weapons;

public class Mage extends Character {


    // initialising the attributes at first (starting numbers before levelling up)
    protected double starting_strength = 1.0;
    protected double starting_dexterity = 1.0;
    protected double starting_intelligence = 8.0;

    //constructor with the keyword super which is referring to the superclass (Character). Used to call superclass methods and to access the superclass constructor

    /**
     * @param name
     * @param characterLvl
     * @param charactersDPS
     * @param basePrmy_attributes
     * @param totalPrimaryAttributes
     * @param weapons
     * @param armor
     */
    public Mage(String name, int characterLvl, double charactersDPS, BasePrmy_attributes basePrmy_attributes, TotalPrimaryAttributes totalPrimaryAttributes, Weapons weapons, Armor armor) {
        super(name, characterLvl, charactersDPS, basePrmy_attributes, totalPrimaryAttributes, weapons, armor);
    }


    //base_Lvl method which take the numbers and sets it to the basePrmy_attributes (
    public void baseLvl() {
        basePrmy_attributes.setStrength(starting_strength);
        basePrmy_attributes.setDexterity(starting_dexterity);
        basePrmy_attributes.setIntelligence(starting_intelligence);
        getWeapons().setRequiredLvl(2);
    }

    //Lvl_up method which adds the number 1, 1, 5 to the basePrmy_attributes and to each attribute(Strength, dexterity & intelligence)
    //this must also pass the if statements
    //
    @Override
    public void lvl_up() {
        characterLvl++;

        basePrmy_attributes.setStrength(basePrmy_attributes.getStrength() + 1);
        basePrmy_attributes.setDexterity(basePrmy_attributes.getDexterity() + 1);
        basePrmy_attributes.setIntelligence(basePrmy_attributes.getIntelligence() + 5);

        if (getWeapons().getEquipmentsWeapon().size() > 0) {
            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Staffs)) {
                WeaponTypes.Staffs.setDamage(WeaponTypes.Staffs.getDamage() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100 * WeaponTypes.Staffs.getAttackPerSec());
                charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getIntelligence() / 100);
                charactersDPS = (charactersDPS * 100) / 100;
            } else {
                WeaponTypes.Wands.setDamage(WeaponTypes.Wands.getDamage() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100 * WeaponTypes.Wands.getAttackPerSec());
                charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getIntelligence() / 100);
                charactersDPS = (charactersDPS * 100) / 100;
            }
        }
//Basically if the attributes are equal to 0 then set the totalprimary attributes by getting it from the basePrmy_attributes
        //else just add the numbers 1, 1, 5 to each attribute
        if (this.getTotalPrimaryAttributes().getStrength() == 0 &&
                this.getTotalPrimaryAttributes().getDexterity() == 0 &&
                this.getTotalPrimaryAttributes().getIntelligence() == 0) {
            totalPrimaryAttributes.setStrength(basePrmy_attributes.getStrength());
            totalPrimaryAttributes.setDexterity(basePrmy_attributes.getDexterity());
            totalPrimaryAttributes.setIntelligence(basePrmy_attributes.getIntelligence());
        } else {
            this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + 1);
            this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + 1);
            this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + 5);


        }


        System.out.println("You leveled up");

    }

    //checks the size first. if right armor is chosen then do the for loop and also check if character is level 2 or more. The stream().map basically allows you to convert object to something else
    // throws exception if the newly added armor attribute is invalid or if level is too low.
    @Override
    public void addArmorAttributes() throws InvalidArmorException {
        armor.getEquipmentsArmor().put(Equipments.Head, ArmorTypes.Cloth);
        armor.getEquipmentsArmor().put(Equipments.Body, ArmorTypes.Cloth);
        armor.getEquipmentsArmor().put(Equipments.Legs, ArmorTypes.Cloth);

        if (getName().equals("Mage") && getCharacterLvl() >= 2 &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Leather) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Mail) &&
                !getArmor().getEquipmentsArmor().containsKey(Equipments.Weapon) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Plate)) {
            for (int j = 0; j < getArmor().getEquipmentsArmor().size(); j++) {
                this.getArmor().getBasePrmy_attributes().setStrength(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getStrength).sum());
                this.getArmor().getBasePrmy_attributes().setDexterity(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getDexterity).sum());
                this.getArmor().getBasePrmy_attributes().setIntelligence(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getIntelligence).sum());
            }
        } else {
            throw new InvalidArmorException("Your level is too low or You cannot get that armor, " +
                    "Mage can only have: " + ArmorTypes.Cloth);

        }
    }

    //This method is basically just adding the armor (cloth) and setting the new character's power based on the armor the character just gained
    //if user chooses another armor other than Cloth then an exception is shown with a message
    @Override
    public void totalAttSum() {
        if (getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Cloth)) {
            for (int i = 0; i < getArmor().getEquipmentsArmor().size(); i++) {
                this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + 1);
                this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + 1);
                this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + 8);
            }
        } else {
            throw new NullPointerException("mage can only own: " + ArmorTypes.Cloth);
        }
    }

    //Mage can only use staff and Wand as weapons. Again character has to be on level 2 or more. Character cannot get other weapons and armors other than the weapon
    //    //Determines whether the added weapons attribute is true, and if so, the determines the weaponsDPS using the value of the added weapon. If the weapon property added is Invalid, an exception is thrown
    @Override
    public void addWeaponsAttributes() throws InvalidWeaponException {
        weapons.getEquipmentsWeapon().put(Equipments.Weapon, WeaponTypes.Staffs);

        if (getName().equals("Mage") && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Axes)
                && getCharacterLvl() >= 2
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Bows)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Hammers)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Daggers)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Body)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Legs)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Head)) {

            // if the staff is chosen then do the following calculation DPS = Damage * Attackspeed
            //else if the other weapon is chosen then do the same calculation to the DPS
            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Staffs)) {
                double WeaponValue = weapons.getWeaponDPS();
                weapons.setWeaponDPS(WeaponTypes.Staffs.getDamage() * WeaponTypes.Staffs.getAttackPerSec() + WeaponValue);
                //   weapons.setWeaponDPS(weapons.getWeaponDPS() * 100) / 100;
            } else {
                double WeaponValue = weapons.getWeaponDPS();
                weapons.setWeaponDPS(WeaponTypes.Wands.getDamage() * WeaponTypes.Wands.getAttackPerSec() + WeaponValue);
                //     weapons.setWeaponDPS(weapons.getWeaponDPS() * 100) / 100;

                // or just throw an exception if users level is too low or user choosing wrong weapon
            }
        } else {
            throw new InvalidWeaponException("Your level is too low or you are probably choosing a wrong weapon, Mage can only use: "
                    + WeaponTypes.Staffs + "and" + WeaponTypes.Wands + ".This are the only ones" + Equipments.Weapon);
        }

    }

    //Basically checking the characters attributes by first checking if the size is equal to 0. If it is then initialise it to 1 and then do the following calculation to the result
    @Override
    public void checkingCharactersAttributes() {
        if (weapons.getEquipmentsWeapon().size() == 0) {
            weapons.setWeaponDPS(1);

        }
    }

    // getting total Characters Damage per second by doing the following calculations
    @Override
    public void totalCharactersDPS() {

        getBasePrmy_attributes().setStrength(starting_strength);
        getBasePrmy_attributes().setDexterity(starting_dexterity);
        getBasePrmy_attributes().setIntelligence(starting_intelligence);

        if (getArmor().getEquipmentsArmor().size() == 0) {
            charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getStrength() / 100);
        } else {
            charactersDPS = weapons.getWeaponDPS() * (1 +
                    (getBasePrmy_attributes().getStrength() + getArmor().getBasePrmy_attributes().getStrength()) / 100);
            charactersDPS = Math.round((charactersDPS * 100) / 100.0);

        }
        System.out.println(charactersDPS);


    }

    // A toString returns the string representation.
    // And by adding @override then we can return the values of the objects
    @Override
    public String toString() {
        return "Mage{" +
                "name='" + name + '\'' +
                ", characterLvl=" + characterLvl +
                ", basePrmy_attributes=" + basePrmy_attributes +
                ", totalPrimaryAttributes=" + totalPrimaryAttributes +
                ", weapons=" + weapons +
                ", CharactersDPS=" + charactersDPS +
                ", armor=" + armor +
                '}';
    }
}










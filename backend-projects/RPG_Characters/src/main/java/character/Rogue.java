package character;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Types.ArmorTypes;
import Types.Equipments;
import Types.WeaponTypes;
import items.Armor;
import items.Weapons;

public class Rogue extends Character {

    // initialising the attributes at first (starting numbers before levelling up)

    protected double starting_strength = 2.0;
    protected double starting_dexterity = 6.0;
    protected double starting_intelligence = 1.0;

    //constructor with the keyword super which is referring to the superclass (Character). Used to call superclass methods and to access the superclass constructor

    public Rogue(String name, int characterLvl, double charactersDPS, BasePrmy_attributes basePrmy_attributes, TotalPrimaryAttributes totalPrimaryAttributes, Weapons weapons, Armor armor) {
        super(name, characterLvl, charactersDPS, basePrmy_attributes, totalPrimaryAttributes, weapons, armor);
    }

    //base_Lvl method which take the numbers and sets it to the basePrmy_attributes (

    public void baseLvl() {
        basePrmy_attributes.setStrength(starting_strength);
        basePrmy_attributes.setDexterity(starting_dexterity);
        basePrmy_attributes.setIntelligence(starting_intelligence);
        getWeapons().setRequiredLvl(2);
    }

    //Lvl_up method which adds the number 1, 4, 1 to the basePrmy_attributes and to each attribute(Strength, dexterity & intelligence)
    //this must also pass the if statements
    @Override
    public void lvl_up() {
        characterLvl++;

        basePrmy_attributes.setStrength(basePrmy_attributes.getStrength() + 1);
        basePrmy_attributes.setDexterity(basePrmy_attributes.getDexterity() + 4);
        basePrmy_attributes.setIntelligence(basePrmy_attributes.getIntelligence() + 1);

        if (getWeapons().getEquipmentsWeapon().size() > 0) {
            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords)) {
                WeaponTypes.Swords.setDamage(WeaponTypes.Swords.getDamage() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100 * WeaponTypes.Swords.getAttackPerSec());
                charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getIntelligence() / 100);
                charactersDPS = (charactersDPS * 100) / 100;
            } else {
                WeaponTypes.Daggers.setDamage(WeaponTypes.Daggers.getDamage() + 5 / 100);
                weapons.setWeaponDPS(weapons.getWeaponDPS() + 5 / 100 * WeaponTypes.Daggers.getAttackPerSec());
                charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getIntelligence() / 100);
                charactersDPS = (charactersDPS * 100) / 100;
            }
        }
        //Basically if the attributes are equal to 0 then set the totalprimary attributes by getting it from the basePrmy_attributes
        //else just add the numbers 1, 4, 1 to each attribute
        if (this.getTotalPrimaryAttributes().getStrength() == 0 &&
                this.getTotalPrimaryAttributes().getDexterity() == 0 &&
                this.getTotalPrimaryAttributes().getIntelligence() == 0) {
            totalPrimaryAttributes.setStrength(basePrmy_attributes.getStrength());
            totalPrimaryAttributes.setDexterity(basePrmy_attributes.getDexterity());
            totalPrimaryAttributes.setIntelligence(basePrmy_attributes.getIntelligence());
        } else {
            this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + 1);
            this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + 4);
            this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + 1);


        }


        System.out.println("You leveled up");

    }

    @Override
    public void addArmorAttributes() throws InvalidArmorException {
        armor.getEquipmentsArmor().put(Equipments.Head, ArmorTypes.Mail);
        armor.getEquipmentsArmor().put(Equipments.Legs, ArmorTypes.Leather);
        armor.getEquipmentsArmor().put(Equipments.Body, ArmorTypes.Leather);

        if (getName().equals("Rogue") && getCharacterLvl() >= 2 &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Plate) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Cloth) &&
                !getArmor().getEquipmentsArmor().containsKey(Equipments.Weapon)) {
            for (int j = 0; j < getArmor().getEquipmentsArmor().size(); j++) {
                this.getArmor().getBasePrmy_attributes().setStrength(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getStrength).sum());
                this.getArmor().getBasePrmy_attributes().setDexterity(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getDexterity).sum());
                this.getArmor().getBasePrmy_attributes().setIntelligence(getArmor().getEquipmentsArmor().values().stream().mapToDouble(ArmorTypes::getIntelligence).sum());
            }
        } else {
            throw new InvalidArmorException("Your level is too low or You cannot get that armor, " +
                    "Rogue can only have: " + ArmorTypes.Mail +
                    ArmorTypes.Leather);

        }
    }

    //This method is basically just adding the armor and setting the new character's power based on the armor the character just gained
    //if user chooses another armor other than Mail and Leather then an exception is shown with a message
    @Override
    public void totalAttSum() {
        if (!getArmor().getEquipmentsArmor().containsKey(Equipments.Weapon) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Plate) &&
                !getArmor().getEquipmentsArmor().containsValue(ArmorTypes.Cloth)) {
            for (int i = 0; i < getArmor().getEquipmentsArmor().size(); i++) {
                this.getTotalPrimaryAttributes().setStrength(totalPrimaryAttributes.getStrength() + 2);
                this.getTotalPrimaryAttributes().setDexterity(totalPrimaryAttributes.getDexterity() + 6);
                this.getTotalPrimaryAttributes().setIntelligence(totalPrimaryAttributes.getIntelligence() + 1);
            }
        } else {
            throw new NullPointerException("Rogue can only own: " + ArmorTypes.Mail + "and" + ArmorTypes.Leather);
        }
    }

    //Determines whether the added weapons attribute is true, and if so, the determines the weaponsDPS using the value of the added weapon. If the weapon property added is Invalid, an exception is thrown

    @Override
    public void addWeaponsAttributes() throws InvalidWeaponException {
        weapons.getEquipmentsWeapon().put(Equipments.Weapon, WeaponTypes.Daggers);

        if (getName().equals("Rogue") && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Axes)
                && getCharacterLvl() >= 2
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Bows)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Hammers)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Staffs)
                && !getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Wands)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Legs)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Head)
                && !getWeapons().getEquipmentsWeapon().containsKey(Equipments.Body)) {

            if (getWeapons().getEquipmentsWeapon().containsValue(WeaponTypes.Swords)) {
                double WeaponValue = weapons.getWeaponDPS();
                weapons.setWeaponDPS(WeaponTypes.Swords.getDamage() * WeaponTypes.Swords.getAttackPerSec() + WeaponValue);
/*
                weapons.setWeaponDPS(weapons.getWeaponDPS() * 100) / 100);
*/

            } else {
                double WeaponValue = weapons.getWeaponDPS();
                weapons.setWeaponDPS(WeaponTypes.Daggers.getDamage() * WeaponTypes.Daggers.getAttackPerSec() + WeaponValue);
/*
                weapons.setWeaponDPS(weapons.getWeaponDPS() * 100) / 100);
*/
            }
        } else {
            throw new InvalidWeaponException("Your level is too low or you are probably choosing a wrong weapon, Rogue can only use: "
                    + WeaponTypes.Swords + Equipments.Weapon);
        }

    }

    @Override
    public void checkingCharactersAttributes() {
        if (weapons.getEquipmentsWeapon().size() == 0) {
            weapons.setWeaponDPS(1);
            /* weapons.weaponDPS = (weapons.weaponDPS * 100)/100;*/
        }
    }

    @Override
    public void totalCharactersDPS() {

        getBasePrmy_attributes().setStrength(starting_strength);
        getBasePrmy_attributes().setDexterity(starting_dexterity);
        getBasePrmy_attributes().setIntelligence(starting_intelligence);

        if (getArmor().getEquipmentsArmor().size() == 0) {
            charactersDPS = weapons.getWeaponDPS() * (1 + getBasePrmy_attributes().getStrength() / 100);
        } else {
            charactersDPS = weapons.getWeaponDPS() * (1 +
                    (getBasePrmy_attributes().getStrength() + getArmor().getBasePrmy_attributes().getStrength()) / 100);
            charactersDPS = Math.round((charactersDPS * 100) / 100.0);

        }
        System.out.println(charactersDPS);


    }

    // A toString returns the string representation.
    // And by adding @override then we can return the values of the objects
    @Override
    public String toString() {
        return "Rogue{" +
                "name='" + name + '\'' +
                ", characterLvl=" + characterLvl +
                ", basePrmy_attributes=" + basePrmy_attributes +
                ", totalPrimaryAttributes=" + totalPrimaryAttributes +
                ", weapons=" + weapons +
                ", CharactersDPS=" + charactersDPS +
                ", armor=" + armor +
                '}';
    }
}
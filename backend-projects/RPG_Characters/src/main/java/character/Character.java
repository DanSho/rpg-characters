package character;

import Attributes.BasePrmy_attributes;
import Attributes.TotalPrimaryAttributes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapons;

public abstract class Character {

    //Field state which are protected because we do not want other users to be able to create instances of the class outside the package
    protected String name;
    protected int characterLvl = 1;
    protected BasePrmy_attributes basePrmy_attributes;

    protected TotalPrimaryAttributes totalPrimaryAttributes;

    protected Weapons weapons;

    protected double charactersDPS;

    protected Armor armor;

    //Constructor
    public Character(String name, int characterLvl, double charactersDPS, BasePrmy_attributes basePrmy_attributes,
                     TotalPrimaryAttributes totalPrimaryAttributes, Weapons weapons, Armor armor) {
        this.name = name;
        this.characterLvl = characterLvl;
        this.charactersDPS = charactersDPS;
        this.basePrmy_attributes = basePrmy_attributes;
        this.totalPrimaryAttributes = totalPrimaryAttributes;
        this.weapons = weapons;
        this.armor = armor;
    }



    /*public abstract  void getTotalAttributes();*/

    //Abstract classes - which has no body, the body is provided by the subclass which we will see in the heroes classes.
    public abstract void lvl_up();

    public abstract void addArmorAttributes() throws InvalidArmorException;

    public abstract void addWeaponsAttributes() throws InvalidWeaponException;

    public abstract void totalAttSum();

    public abstract void checkingCharactersAttributes();

    public abstract void totalCharactersDPS();


    public void gain_lvl() {
        this.characterLvl++;

    }

    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCharacterLvl() {
        return characterLvl;
    }

    public void setCharacterLvl(int characterLvl) {
        this.characterLvl = characterLvl;
    }

    public BasePrmy_attributes getBasePrmy_attributes() {
        return basePrmy_attributes;
    }

    public void setBasePrmy_attributes(BasePrmy_attributes basePrmy_attributes) {
        this.basePrmy_attributes = basePrmy_attributes;
    }

    public TotalPrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    public void setTotalPrimaryAttributes(TotalPrimaryAttributes totalPrimaryAttributes) {
        this.totalPrimaryAttributes = totalPrimaryAttributes;
    }

    public Weapons getWeapons() {
        return weapons;
    }

    public void setWeapons(Weapons weapons) {
        this.weapons = weapons;
    }

    public double getCharactersDPS() {
        return charactersDPS;
    }

    public void setCharactersDPS(double charactersDPS) {
        this.charactersDPS = charactersDPS;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }

    // A toString returns the string representation.
    // And by adding @override then we can return the values of the objects

    public void displayCharacter() {
        StringBuilder stats = new StringBuilder();
        stats.append("\n Name: " + getName() + "CharacterLevel: " + getCharacterLvl() + ")\n");
        stats.append("===========\n");
        stats.append("\n Strength: ").append(getTotalPrimaryAttributes().getStrength());
        stats.append("\n Dexterity: ").append(getTotalPrimaryAttributes().getDexterity());
        stats.append("\n Intelligence: " + getTotalPrimaryAttributes().getIntelligence());
        stats.append("\n DPS: " + getCharactersDPS() + "\n");
        System.out.println(stats);

    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", characterLvl=" + characterLvl +
                ", basePrmy_attributes=" + basePrmy_attributes +
                ", totalPrimaryAttributes=" + totalPrimaryAttributes +
                ", weapons=" + weapons +
                ", CharactersDPS=" + charactersDPS +
                ", armor=" + armor +
                '}';
    }
}

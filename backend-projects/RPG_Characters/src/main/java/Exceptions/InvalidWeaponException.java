package Exceptions;

public class InvalidWeaponException extends Exception {
    public String message;

    public InvalidWeaponException(String message) {
        this.message = message;

    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "InvalidWeaponException{" +
                "message='" + message + '\'' +
                '}';
    }
}


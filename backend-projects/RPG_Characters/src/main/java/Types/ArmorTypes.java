package Types;

public enum ArmorTypes {
    Cloth("Cloth", 3, 2, 4),
    Leather("Leather", 3, 2, 1),
    Mail("Mail", 1, 1, 3),
    Plate("Plate", 6, 1, 1);


    String Equipments;
    double strength;
    double dexterity;
    double intelligence;

    ArmorTypes(String equipments, double strength, double dexterity, double intelligence) {
        Equipments = equipments;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public String getEquipments() {
        return Equipments;
    }

    public void setEquipments(String equipments) {
        Equipments = equipments;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public void setDexterity(double dexterity) {
        this.dexterity = dexterity;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }


}

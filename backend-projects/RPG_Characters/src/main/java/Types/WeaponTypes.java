package Types;

public enum WeaponTypes {

    //Conerverting enums to toStrings

    Axes("Axes", 20, 9),
    Bows("Bows", 10, 5),
    Daggers("Daggers", 13, 4),
    Hammers("Hammers", 22, 6),
    Staffs("Staffs", 19, 9),
    Swords("Swords", 25, 15),
    Wands("Wands", 21, 11);

//Initialising equipments as string and damage, attackperSec as ints
    String equipments;
    double damage;
    double attackPerSec;

//Constructors
    WeaponTypes(String equipments, double damage, double attackPerSec) {
        this.equipments = equipments;
        this.damage = damage;
        this.attackPerSec = attackPerSec;
    }
//Getter & Setters
    public String getEquipments() {
        return equipments;
    }

    public void setEquipments(String equipments) {
        this.equipments = equipments;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackPerSec() {
        return attackPerSec;
    }

    public void setAttackPerSec(double attackPerSec) {
        this.attackPerSec = attackPerSec;
    }

    // A toString returns the string representation.
    // And by adding @override then we can return the values of the objects
    @Override
    public String toString() {
        return "WeaponTypes{" +
                "equipments='" + equipments + '\'' +
                ", damage=" + damage +
                ", attackPerSec=" + attackPerSec +
                '}';
    }
}

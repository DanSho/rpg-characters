## Background

This is a Java application which consist of 4 heroes. The basic goal of the game is to allow a user to create a hero by filling out the necessary areas. Once you have a hero you should be able to purchase armor and weapons that are of the same level as the hero. The user should be able to level up as well and once the user is either on level 2 or above, then the user should be able to purchase armor and/or weapons. Basically each character can be strengthened and changed. 


## Install 

I had to install Maven to do the tests.

## Run 

You must use System.out.println() to start the game, which should contain the chosen character inside the parenthesis so that all the method related to the character may be invoked. For example: 

 warrior.baseLvl();
   -----> System.out.println(warrior); <------
warrior.lvl_up();
   -----> System.out.println(warrior);; <-----
 warrior.totalCharactersDPS();
 System.out.println(warrior);
  warrior.addArmorAttributes();
  warrior.totalAttSum();
  warrior.addWeaponsAttributes();
----> System.out.println(warrior); <------
warrior.lvl_up();




## Usage 

#Each hero has a seperate class in the game that extends the functionality of the abstract character class, which provides abstract methods. Also 2 exception classes that are thrown when necessary. One for the armor and the other for the weapon. 
#The classes Armor and Weapons inherit from the abstract class Item in the game.

#SetWeapon equips the hero with a weapon. However, you can only obtain a certain sort of weapon depending on the hero you choose to be. To obtain it, you must also be at the same level as the weapon or greater as I have mentioned earlier. The weapon you build will be saved in the "hashmap". The existing weapon will be swapped out for the new one if you change weapons. 

#SetArmor basically sets the hero's armor. But no matter whatever hero you pick, you can only use specific kinds of armor. You should be at least as powerful as the armor. In other words, you need to be on either level 2 or more. 

## Test

The complete full test coverage is in my project with functionality using JUnit 5.

## Class Diagram
ClassDiagram.png


## Maintainers 

@DanSho Daniel Sholaja
     
